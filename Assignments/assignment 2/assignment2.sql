create or replace package book_store AS
    FUNCTION get_price_after_tax(book_isbn varchar2)
        return NUMBER;
end book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS

    FUNCTION price_after_discount(book_isbn VARCHAR2)
        return NUMBER IS
            Discount_price NUMBER(5,2);
        BEGIN
            SELECT (b.retail - b.DISCOUNT) INTO Discount_price FROM books b
            WHERE b.ISBN = book_isbn;
            return Discount_price;
        end;

    FUNCTION get_price_after_tax(book_isbn varchar2)
        RETURN NUMBER IS
            final_cost NUMBER;
            Discount_price NUMBER;
        BEGIN
            discount_price := price_after_discount(book_isbn);
            final_cost := discount_price + (Discount_price * 0.15);
            RETURN final_cost;
        
    END;

END book_store;
/

DECLARE
    result NUMBER(5,2);
    book_isbn NUMBER(10);
BEGIN
    SELECT b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'BUILDING A CAR WITH TOOTHPICKS';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of BUILDING A CAR WITH TOOTHPICKS: ' || result);

    SELECT b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'HOLY GRAIL OF ORACLE';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of HOLY GRAIL OF ORACLE: ' || result);
end;
/
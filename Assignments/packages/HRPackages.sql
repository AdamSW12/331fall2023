create or replace PACKAGE hr_packages AS
    FUNCTION get_dept_id(dept_name varchar2)
        return NUMBER;
    FUNCTION get_city (dept_name varchar2)
        return varchar2;
    PROCEDURE add_dept_manager(dept_name IN varchar2, man_id IN number, dept_id OUT number);
END hr_packages;
/

create or replace package body hr_packages AS

    FUNCTION get_dept_id(dept_name VARCHAR2)
        RETURN NUMBER IS
            DEPT_ID NUMBER;
        BEGIN
            SELECT D.DEPARTMENT_ID into DEPT_ID FROM hr.DEPARTMENTS D
            where D.DEPARTMENT_NAME = dept_name;
            RETURN DEPT_ID;
        END;
    FUNCTION get_city(dept_name varchar2)
        RETURN VARCHAR2 IS
            CITY VARCHAR2(30);
        BEGIN  
            SELECT L.city INTO city FROM hr.LOCATIONS L
            INNER JOIN hr.DEPARTMENTS D USING (LOCATION_ID)
            WHERE D.DEPARTMENT_NAME = dept_name;
            RETURN CITY;
        end;

    PROCEDURE add_dept_manager(dept_name IN varchar2, man_id IN number, dept_id OUT number) IS
        BEGIN
            UPDATE DEPARTMENTS
            SET MANAGER_ID = man_id
            WHERE DEPARTMENT_NAME = dept_name;

            SELECT d.DEPARTMENT_ID INTO DEPT_ID FROM departments D
            WHERE d.MANAGER_ID = man_id;
    end;

END hr_packages;
/

DECLARE
    result number(4);
BEGIN
    result := hr_packages.get_dept_id('Marketing');
    DBMS_output.put_line('Marketing id: ' || result);
end;
/

DECLARE
    result VARCHAR2(30);
BEGIN
    result := hr_packages.get_city('Marketing');
    DBMS_output.put_line('Marketing city: ' || result);
end;
/

DECLARE 
    dept_name varchar2(30) := 'Administration';
    man_id number(6) := 300;
    dept_id number(4);
BEGIN
    HR_PACKAGES.add_dept_manager(dept_name, man_id, DEPT_ID);
    DBMS_OUTPUT.PUT_LINE(DEPT_ID);
end;
/

SELECT E.first_name || ' ' || E.LAST_NAME AS "Full Name" FROM hr.EMPLOYEES E 
WHERE E.DEPARTMENT_ID = HR_PACKAGES.GET_DEPT_ID('Marketing');

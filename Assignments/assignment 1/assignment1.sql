--drop tables
    drop table Person;
    drop table Occupation;
    drop table Home;

-- creating tables
CREATE table Home(
    hid char(4) primary KEY,
    address varchar2(200) NOT null
);
CREATE table Occupation(
    oid char(4) primary key,
    type varchar2(20),
    salary number(10,2)
);
create table Person(
    pid char(4) PRIMARY key,
    firstname varchar2(20) NOT null,
    lastname varchar2(20) NOT null,
    father_id char(4) references person(pid),
    mother_id char(4) references person(pid),
    hid char(4) references home(hid),
    oid char(4) REFERENCES Occupation(oid)
);

--inserts
insert into Home VALUES ('H001','123 easy St.');
insert into Home VALUES ('H002','56 Fake Ln.');

insert into Occupation VALUES ('O000','N/A',0);
insert into Occupation VALUES ('O001','Student',0);
insert into Occupation VALUES ('O002', 'Doctor', 100000);
insert into Occupation VALUES ('O003', 'Professor', 80000);

insert into Person VALUES ('P001','Zachary','Aberny',null,null,'H002',null);
insert into Person VALUES ('P002','Yanni','Aberny',null,null,'H002',null);
insert into Person VALUES ('P003','Alice','Aberny','P001','P002','H001','O002');
insert into Person VALUES ('P004','Bob','Bortelson',null,null,'H001','O003');
insert into Person VALUES ('P005','Carl','Aberny-Bortelson','P004','P003','H001','O001');
insert into Person VALUES ('P006','Denise','Aberny-Bortelson','P004','P003','H001','O001');

--test query
SELECT gp.firstname FROM person gp 
JOIN person p ON gp.pid = p.mother_id OR gp.pid = p.father_id 
JOIN person c ON p.pid = c.mother_id OR p.pid = c.father_id 
WHERE c.firstname = 'Denise';

--Queries

--1 total residents
Select H.address, Count(P.firstname) AS "Number of residents" from Home H
Inner join Person P ON P.hid = H.hid
Group by h.address;

--2 full name of all grandfathers
Select Unique GF.firstname || ' ' || GF.lastname AS "GrandFather Name" From person GF
inner join person p on GF.pid = p.father_id
inner join person c on c.father_id = p.pid or c.mother_id = p.pid;

--3 full name of students that live with at least 1 parent
Select Unique S.firstname || ' ' || S.lastname AS "Student Full Name" From person S 
inner join person P ON s.father_id = p.pid OR s.mother_id = p.pid 
where S.oid = 'O001' AND s.hid = p.hid;

--4 highest household income
Select MAX(Sum(O.salary)) AS "Highest Household income" from Home H
Inner join Person P ON P.hid = H.hid
Inner join Occupation O on o.oid = p.oid
Group by h.address;
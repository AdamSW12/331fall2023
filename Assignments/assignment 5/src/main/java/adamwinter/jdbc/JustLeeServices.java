package adamwinter.jdbc;
import java.sql.*;
import java.util.Scanner;

public class JustLeeServices 
{
    public static void main( String[] args ) throws SQLException
    {
        // PreparedStatement statement = conn.prepareStatement("select * from books where ISBN = ?");
        // statement.setString(1, "1059831198");
        // ResultSet result = statement.executeQuery();
        // while(result.next()){
        //     System.out.println(result.getDate("pubdate"));
        // }
        Connection conn = getConnection();

        //create publisher for the new book
        Publisher pub = new Publisher();
        PreparedStatement publisherStatement = conn.prepareStatement("select * from publisher where pubid = 6");
        ResultSet pubResults = publisherStatement.executeQuery();
        while (pubResults.next()) {
            pub = new Publisher(pubResults.getDouble("pubid"), pubResults.getString("name"),
                    pubResults.getString("contact"), pubResults.getString("phone"), conn);
        }

        // Book book = getBook("1059831198", conn);
        // System.out.println(book);
        
        Book newBook = new Book("0375703764", "HOUSE OF LEAVES", pub, Date.valueOf("2000-03-07"), 50, 60, 0, "HORROR");
        if(bookExists(newBook,conn)){
            removeBook(newBook, conn);
        }

        addBook(newBook, conn);

        System.out.println(getBook(newBook.getISBN(),conn));

        if (!publisherStatement.isClosed()){
            publisherStatement.close();
        }
        if (!conn.isClosed()) {
            conn.close();
        }
    }

    public static Book getBook(String ISBN,Connection conn) throws SQLException{
        Publisher pub = new Publisher();
        Book book = new Book();

        PreparedStatement statement = conn.prepareStatement("select * from books where ISBN = ?");
        statement.setString(1, ISBN);
        ResultSet result = statement.executeQuery();

        PreparedStatement publisherStatement = conn.prepareStatement("select * from publisher where pubid = ?");
        while(result.next()){
            publisherStatement.setDouble(1,result.getDouble("pubid"));
        }
        
        ResultSet pubResults = publisherStatement.executeQuery();

        while(pubResults.next()){
            pub = new Publisher(pubResults.getDouble("pubid"),pubResults.getString("name"),pubResults.getString("contact"),pubResults.getString("phone"),conn);
        }

        result = statement.executeQuery();
        while(result.next())
        {

            book = new Book(
                ISBN,
                result.getString("title"),
                pub,
                result.getDate("pubDate"),
                result.getDouble("cost"),
                result.getDouble("retail"),
                result.getDouble("discount"),
                result.getString("category")
            );
        }
        if(!statement.isClosed())
        {
            statement.close();
        }

        return book;

    }

    public static void addBook(Book book,Connection conn) throws SQLException{
        PreparedStatement addBookStatement = conn.prepareStatement("Insert into books VALUES(?,?,?,?,?,?,?,?)");
        addBookStatement.setString(1, book.getISBN());
        addBookStatement.setString(2, book.getTitle());
        addBookStatement.setDate(3, book.getPubDate());
        addBookStatement.setDouble(4, book.getPubID());
        addBookStatement.setDouble(5, book.getCost());
        addBookStatement.setDouble(6, book.getRetail());
        if(book.getDiscount() == 0){
            addBookStatement.setNull(7,0);
        }else{
            addBookStatement.setDouble(7, book.getDiscount());
        }
        addBookStatement.setString(8, book.getCategory());

        addBookStatement.executeUpdate();

    }

    public static void removeBook(Book book, Connection conn) throws SQLException{
        PreparedStatement removeBookStatement = conn.prepareStatement("DELETE FROM BOOKS where isbn = ?");
        removeBookStatement.setString(1,book.getISBN());

        removeBookStatement.executeUpdate();
    }

    public static boolean bookExists(Book book, Connection conn) throws SQLException{
        PreparedStatement statement = conn.prepareStatement("select * from books where ISBN = ?");
        statement.setString(1, book.getISBN());
        ResultSet result = statement.executeQuery();

        if(result.next()){
            return true;
        }
        return false;
    }

    public static Connection getConnection() throws SQLException{
        Scanner in = new Scanner(System.in);
        System.out.print("Username: ");
        String userName = in.next();

        System.out.print("Password: ");
        String userPWD = in.next();

        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn =
        DriverManager.getConnection(url, userName, userPWD);

        in.close();
        return conn;
    }
}

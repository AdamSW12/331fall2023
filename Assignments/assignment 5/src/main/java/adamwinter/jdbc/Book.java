package adamwinter.jdbc;
import java.sql.*;
import java.sql.Date;

public class Book {
    
    private String ISBN;
    private String title;
    private Date pubDate;
    private Publisher publisher;
    private double cost;
    private double pubID;
    private double retail;
    private double discount;
    private String category;

    public Book(String ISBN, String title, Publisher publisher,Date pubDate, double cost, double retail, double discount,String category) throws SQLException{

        checkData(ISBN,10);
        checkData(title,30);
        checkData(category,12);
        checkData(cost);
        checkData(retail);
        checkData(discount);

        this.ISBN = ISBN;
        this.title = title;
        this.pubDate = pubDate;
        this.publisher = publisher;
        this.pubID = publisher.getPubID();
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
    }

    public Book(){}
    private void checkData(String data, int limit){
        if(data.length()<=limit){
            return;
        }
        throw new IllegalArgumentException(data + " too long, max length: " + data); 
    }

    private void checkData(double data){
        if(data<=999){
            return;
        }
        throw new IllegalArgumentException(data + " too big of a number, max: 999"); 
    }

    public String getISBN() {
        return ISBN;
    }

    public String getTitle() {
        return title;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public double getCost() {
        return cost;
    }

    public double getPubID() {
        return pubID;
    }

    public double getRetail() {
        return retail;
    }

    public double getDiscount() {
        return discount;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Book [ISBN=" + this.ISBN + 
        ", title=" + this.title + 
        ", date=" + this.pubDate + 
        ", pubID=" + this.pubID + 
        ", cost=" + this.cost + 
        ", retail=" + this.retail + 
        ", discount=" + this.discount + 
        ", category=" + this.category + "]";
    }

}

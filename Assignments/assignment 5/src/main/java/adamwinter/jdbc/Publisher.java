package adamwinter.jdbc;
import java.sql.*;

public class Publisher {
    private double pubID;
    private String pubName;
    private String pubContact;
    private String pubPhone;
    private Connection conn;

    public Publisher(Double pubID, String pubName, String pubContact, String pubPhone,Connection conn) throws SQLException {
        pubExists(pubID,conn);
        checkData(pubName,23);
        checkData(pubContact,15);
        checkData(pubPhone,12);

        this.conn = conn;
        this.pubID = pubID;
        this.pubName = pubName;
        this.pubContact = pubContact;
        this.pubPhone = pubPhone;
    }

    public Publisher(){}

    public void pubExists(Double otherid,Connection conn) throws SQLException{
        PreparedStatement statement = conn.prepareStatement("Select pubid from publisher where pubid = ?");
        statement.setDouble(1,otherid);
        ResultSet result = statement.executeQuery();

        if(result.next()){
            return;
        }

        throw new IllegalArgumentException("publisher does not exist");
    }

    private void checkData(String data, int limit){
        if(data.length()<=limit){
            return;
        }
        throw new IllegalArgumentException(data + " too long, max length: " + data); 
    }

    public Double getPubID() {
        return pubID;
    }
    public String getPubName() {
        return pubName;
    }
    public String getPubContact() {
        return pubContact;
    }
    public String getPubPhone() {
        return pubPhone;
    }


}

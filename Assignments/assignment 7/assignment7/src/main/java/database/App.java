package database;
import java.sql.*;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter your username: ");
        String userName = in.next();
        System.out.print("Enter your password: ");
        String userPWD = in.next();

        CourseListService courseList = new CourseListService(userName, userPWD);
        courseList.addCourse();
    }
}

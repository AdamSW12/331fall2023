package database;
import java.sql.*;
public class Courses {

    private String course_number;
    private int term_number;
    private String course_name;
    private String description;
    private int class_hours;
    private int lab_hours;
    private int home_hours;
    private String course_type;

    public Courses(String course_number, int term_number, String        course_name, String description, int class_hours, int lab_hours, int home_hours,String course_type){

        this.course_number = course_number;
        this.term_number = term_number;
        this.course_name = course_name;
        this.description = description;
        this.class_hours = class_hours;
        this.lab_hours = lab_hours;
        this.home_hours = home_hours;
        this.course_type = course_type;
    }

    //getters
    public String getCourse_number() {
        return course_number;
    }

    public int getTerm_number() {
        return term_number;
    }

    public String getCourse_name() {
        return course_name;
    }

    public String getDescription() {
        return description;
    }

    public int getClass_hours() {
        return class_hours;
    }

    public int getLab_hours() {
        return lab_hours;
    }

    public int getHome_hours() {
        return home_hours;
    }

    public String getCourse_type() {
        return course_type;
    }
    
    //setters
    public void setCourse_number(String course_number) {
        this.course_number = course_number;
    }

    public void setTerm_number(int term_number) {
        this.term_number = term_number;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setClass_hours(int class_hours) {
        this.class_hours = class_hours;
    }

    public void setLab_hours(int lab_hours) {
        this.lab_hours = lab_hours;
    }

    public void setHome_hours(int home_hours) {
        this.home_hours = home_hours;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public void addToDatabase(Connection conn){
        try {
            PreparedStatement addCourse = conn.prepareStatement("insert into courses values(?,?,?,?,?,?,?,?)");
            addCourse.setString(1, this.course_number);
            addCourse.setInt(2,this.term_number);
            addCourse.setString(3, this.course_name);
            addCourse.setString(4, this.description);
            addCourse.setInt(5, this.class_hours);
            addCourse.setInt(6, this.lab_hours);
            addCourse.setInt(7, this.home_hours);
            addCourse.setString(8, this.course_type);

            addCourse.executeUpdate();
            if(!addCourse.isClosed()){
                addCourse.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{

        }
        
    }

    //toString
    @Override
    public String toString() {
        return "Courses [course_number=" + course_number + ", term_number=" + term_number + ", course_name="
                + course_name + ", description=" + description + ", class_hours=" + class_hours + ", lab_hours="
                + lab_hours + ", home_hours=" + home_hours + ", course_type=" + course_type + "]";
    }
}

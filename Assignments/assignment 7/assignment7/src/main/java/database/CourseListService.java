package database;

import java.sql.*;
import java.util.Scanner;

public class CourseListService {
    private String userName;
    private String userPWD;
    private Connection conn;

    public CourseListService(String userName,String userPWD){
        this.userName = userName;
        this.userPWD = userPWD;

        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try {
            this.conn =
            DriverManager.getConnection(url, this.userName, this.userPWD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        try {
            if (!conn.isClosed()) {
                conn.close();
            } 
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCourse(){
        Scanner in = new Scanner(System.in);

        System.out.print("Enter course number:");
        String course_number = in.nextLine();

        System.out.println("enter term number (1-6): ");
        int term_number = in.nextInt();

        System.out.println("enter course name:");
        in.nextLine();//consumes extra line from previous method call
        String course_name = in.nextLine();
        System.out.println("Enter course description:");
        String description = in.nextLine();
       
        
        System.out.println("enter the amount of class hours:");
        int class_hours = in.nextInt();
       

        System.out.println("Enter the amount of lab hours:");
        int lab_hours = in.nextInt();

        System.out.println("enter the amount of home hours:");
        int home_hours = in.nextInt();

        System.out.println("Enter the course type (general education OR concentration)");
        String course_type = in.next();

        try {
            Courses course = new Courses(course_number, term_number, course_name, description, class_hours, lab_hours, home_hours, course_type);
            course.addToDatabase(conn);
            
        } catch (Exception e) {
            e.printStackTrace();
        }

        in.close();
        close();
    }
}

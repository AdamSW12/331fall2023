package database;
import java.sql.*;

public class SemesterInfo {
    private int Semester_number;
    private String semester;
    
    public SemesterInfo(int semester_number, String semester) {
        this.Semester_number = semester_number;
        this.semester = semester;
    }
    public int getSemester_number() {
        return this.Semester_number;
    }
    public void setSemester_number(int semester_number) {
        this.Semester_number = semester_number;
    }
    public String getSemester() {
        return this.semester;
    }
    public void setSemester(String semester) {
        this.semester = semester;
    }
    @Override
    public String toString() {
        return "SemesterInfo [Semester_number=" + this.Semester_number + ", semester=" + this.semester + "]";
    }

    public void addToDatabase(Connection conn){
        try {
            PreparedStatement addSemester = conn.prepareStatement("insert into SEMESTER_INFO values(?,?)");
            addSemester.setInt(1, this.Semester_number);
            addSemester.setString(2, this.semester);

            if(!addSemester.isClosed()){
                addSemester.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
}

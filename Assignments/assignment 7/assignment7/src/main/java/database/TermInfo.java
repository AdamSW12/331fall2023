package database;
import java.sql.*;
public class TermInfo {
    private int term_number;
    private int semester_number;

    public TermInfo(int term_number, int semester_number) {
        this.term_number = term_number;
        this.semester_number = semester_number;
    }
    public int getTerm_number() {
        return this.term_number;
    }
    public void setTerm_number(int term_number) {
        this.term_number = term_number;
    }
    public int getSemester_number() {
        return this.semester_number;
    }
    public void setSemester_number(int semester_number) {
        this.semester_number = semester_number;
    }
    @Override
    public String toString() {
        return "TermInfo [term_number=" + this.term_number + ", semester_number=" + this.semester_number + "]";
    }
    public void addToDatabase(Connection conn){
        try {
            PreparedStatement addTerm = conn.prepareStatement("insert into TERM_INFO values(?,?)");
            addTerm.setInt(1, this.term_number);
            addTerm.setInt(2, this.semester_number);

            if(!addTerm.isClosed()){
                addTerm.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    
}

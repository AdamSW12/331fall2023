--table drops
DROP TABLE semester_info CASCADE CONSTRAINTS;
DROP TABLE term_info CASCADE CONSTRAINTS;
DROP TABLE courses CASCADE CONSTRAINTS;

--table creations
create table semester_info(
    semester_number number(1) PRIMARY KEY,
    semester varchar2(6) NOT NULL
);

create table term_info(
    term_number number(1) primary key,
    semester_number number(1) REFERENCES semester_info(semester_number)
);

CREATE TABLE courses
(
    course_number varchar2(10) PRIMARY KEY,
    term_number number(1) REFERENCES term_info(term_number),
    course_name varchar2(50),
    description varchar(500),
    class_hours number(2),
    lab_hours number(2),
    home_hours number(2),
    course_type varchar2(20)
);

--inserts
insert into SEMESTER_INFO 
VALUES(1,'FALL');
insert into SEMESTER_INFO 
values(2,'WINTER');
insert into SEMESTER_INFO 
values(3,'SUMMER');

insert into TERM_INFO
values(1,1);
insert into TERM_INFO
values(2,2);
insert into TERM_INFO
values(3,1);
insert into TERM_INFO
values(4,2);
insert into TERM_INFO
values(5,1);
insert into TERM_INFO
values(6,2);

insert into COURSES
values('420-110-DW',1,'Programming I','The course will introduce the student to the basic building blocks (sequential,
selection and repetitive control structures) and modules (methods and classes)
used to write a program. The student will use the Java programming language to
implement the algorithms studied. The array data structure is introduced, and
student will learn how to program with objects.',3,3,3,'concentration');

insert into COURSES
values('420-210-DW',2,'Programming II','The course will introduce the student to basic object-oriented methodology in
order to design, implement, use and modify classes, to write programs in the
Java language that perform interactive processing, array and string processing,
and data validation. Object-oriented features such as encapsulation and
inheritance will be explored.',3,3,3,'concentration');


-- select c.course_number,s.semester FROM COURSES c
-- INNER JOIN TERM_INFO TI Using(term_number)
-- INNER JOIN SEMESTER_INFO s USING (SEMESTER_NUMBER);


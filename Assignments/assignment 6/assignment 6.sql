--drop tables
DROP TABLE AUTHOR CASCADE CONSTRAINTS;
DROP TABLE BOOKS CASCADE CONSTRAINTS;
DROP TABLE author_books CASCADE CONSTRAINTS;
DROP TABLE AUTHOR_COUNTRY CASCADE CONSTRAINTS;

--create tables
CREATE TABLE AUTHOR(
    AUTHORID VARCHAR2(4) PRIMARY KEY,
    author_name VARCHAR2(30),
    author_city VARCHAR2(30)
);

CREATE TABLE BOOKS(
    ISBN varchar2(6) primary key,
    Title varchar2(40),
    category varchar2(20),
    price number (5,2)
);

CREATE TABLE author_books(
    ISBN varchar2(6) REFERENCES BOOKS(ISBN),
    AUTHORID VARCHAR2(4) REFERENCES AUTHOR(AUTHORID),
        CONSTRAINT book_author_pk PRIMARY KEY(ISBN,AUTHORID)
);

create table author_country(
    city varchar2(30),
    country varchar2(30)
);


--Author inserts
INSERT INTO AUTHOR
VALUES('A100','Reginald Authorson','Montreal');
INSERT INTO AUTHOR
VALUES('T100','XYZ Tolkeen','Birmingham');
INSERT INTO AUTHOR
VALUES('R100','B Ronalds','Birmingham');
INSERT INTO AUTHOR
VALUES('P100','Janeet Paulton','Toronto');
INSERT INTO AUTHOR
VALUES('R200','Vanti Raulson','Toronto');

--author country inserts
INSERT INTO author_country
VALUES('Montreal','Canada');
INSERT INTO author_country
VALUES('Toronto','Canada');
INSERT INTO author_country
VALUES('Birmingham','England');

--Books inserts
INSERT INTO BOOKS
VALUES('123ABC','How to book','Business',20.23);
INSERT INTO BOOKS
VALUES('JKLXCD','Can you really ever book?','Business',30.12);
INSERT INTO BOOKS
VALUES('AJKAJA','Wow hobbits!','Fantasy',10.2);
INSERT INTO BOOKS
VALUES('ASDSAK','Geez its hobbits.','Fantasy',4.5);
INSERT INTO BOOKS
VALUES('AJSHAK','Science adventure','Sci-fi',13.12);
INSERT INTO BOOKS
VALUES('OAISJD','it''s cooking!','cooking',35.36);
INSERT INTO BOOKS
VALUES('ASDOA','The times and ideas of Ronald McRonald','Biography',23);

--author_books inserts
INSERT INTO author_books
VALUES('123ABC','A100');
INSERT INTO author_books
VALUES('JKLXCD','A100');
INSERT INTO author_books
VALUES('AJKAJA','T100');
INSERT INTO author_books
VALUES('ASDSAK','T100');
INSERT INTO author_books
VALUES('AJSHAK','R100');
INSERT INTO author_books
VALUES('OAISJD','P100');
INSERT INTO author_books
VALUES('ASDOA','P100');
INSERT INTO author_books
VALUES('ASDOA','R200');
/


--Queries
    --books over 15$
    select count(b.title) FROM BOOKS b where b.PRICE>15;

    --all fantasy including title and author name
    select b.title,b.category,a.author_name FROM books b 
    inner join author_books ab using (isbn)
    inner join author a using (AUTHORID)
    where b.CATEGORY = 'Fantasy';

    --all canadian book titles
    select UNIQUE b.title FROM books b 
    inner join author_books ab using (isbn)
    inner join author a using (AUTHORID)
    INNER JOIN AUTHOR_COUNTRY ac ON a.AUTHOR_CITY = ac.CITY
    where ac.COUNTRY = 'Canada';

--view
CREATE OR REPLACE VIEW BusinessBooks AS 
    select UNIQUE b.title, b.PRICE, a.AUTHOR_NAME, a.AUTHOR_CITY 
    FROM books b 
    inner join author_books ab using (isbn)
    inner join author a using (AUTHORID)
    where b.CATEGORY = 'Business';
    
    Select * from BusinessBooks;
    --0.017 seconds before index

--index

DROP INDEX bookIndex;
CREATE INDEX bookIndex ON
Books(ISBN,Title,price,category);

DROP INDEX authorIndex;
CREATE INDEX authorIndex ON
Author(Author_name,Author_city,author_country);

Select * from BusinessBooks;
--0.015 seconds after index


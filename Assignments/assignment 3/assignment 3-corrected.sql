
create or replace package book_store AS
    FUNCTION get_price_after_tax(book_isbn varchar2)
        return NUMBER;
    TYPE Order_Array_Type IS VARRAY(100) OF NUMBER;
    PROCEDURE show_purchasers;
end book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS

    FUNCTION price_after_discount(book_isbn VARCHAR2)
        return NUMBER IS
            Discount_price NUMBER(5,2);
        BEGIN
            SELECT (b.retail - b.DISCOUNT) INTO Discount_price FROM books b
            WHERE b.ISBN = book_isbn;
            return Discount_price;
        end;

    FUNCTION get_price_after_tax(book_isbn varchar2)
        RETURN NUMBER IS
            final_cost NUMBER;
            Discount_price NUMBER;
        BEGIN
            discount_price := price_after_discount(book_isbn);
            final_cost := discount_price + (Discount_price * 0.15);
            RETURN final_cost;
        
    END;

    FUNCTION book_purchasers(book_isbn varchar2)
    RETURN Order_Array_Type IS
        Order_Array Order_Array_Type;
    BEGIN
        SELECT unique o.customer# BULK COLLECT INTO Order_array FROM ORDERS o
        INNER JOIN ORDERITEMS ob USING (ORDER#)
        WHERE ob.ISBN = book_isbn;
        return Order_array;
    end;

    PROCEDURE show_purchasers IS
        customer#_arr book_store.Order_Array_Type;
        customer_Name varchar2(30);
    BEGIN
        FOR arow IN (SELECT * FROM BOOKS) LOOP
            CUSTOMER#_arr := BOOK_STORE.book_purchasers(arow.ISBN);
            
            DBMS_OUTPUT.PUT_LINE('-----------------------------------------');
            DBMS_OUTPUT.PUT_LINE('ISBN: ' || arow.isbn || ', Title: ' || arow.title || ', Names: ' );

            FOR i in 1.. customer#_arr.COUNT LOOP

                SELECT unique firstname || ' ' || lastname into customer_Name 
                from customers
                where customer# = customer#_arr(i);
                DBMS_OUTPUT.PUT(customer_Name || ', ');

            end loop;
            DBMS_OUTPUT.NEW_LINE();

        end loop;

    END;
END book_store;
/

DECLARE
    result NUMBER(5,2);
    book_isbn NUMBER(10);
    order_arr BOOK_STORE.Order_Array_Type;
BEGIN
    SELECT UNIQUE b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'BUILDING A CAR WITH TOOTHPICKS';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of BUILDING A CAR WITH TOOTHPICKS: ' || result);

    SELECT UNIQUE b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'HOLY GRAIL OF ORACLE';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of HOLY GRAIL OF ORACLE: ' || result);
    

    BOOK_STORE.show_purchasers;
end;
/       
DROP TABLE JLUSERS; 
DROP TYPE JLUSERS_TYPE;

CREATE TABLE JLUSERS(
    userID VARCHAR2(30) primary key,
    failedLoginCount NUMBER(1),
    salt RAW(30),
    hash RAW(30)
);

create or replace type JLUSERS_TYPE AS OBJECT(
    USERID VARCHAR2(30),
    failedLoginCount NUMBER(1),
    salt RAW(30),
    hash RAW(30)
);
/

create or replace procedure createJLUSER(JLUSER IN JLUSERS_type) AS
    userExists NUMBER;
BEGIN
    SELECT count(*) INTO userExists FROM JLUSERS WHERE userID = JLUSER.userID;

    IF(userExists = 0) THEN
        INSERT INTO JLUSERS
        VALUES(JLUSER.userID,JLUSER.failedLoginCount,JLUSER.salt,JLUSER.hash);
    ELSE
        UPDATE JLUSERS
            SET     userId = JLUSER.userID,
                    failedLoginCount = JLUSER.failedLoginCount,
                    salt = JLUSER.salt,
                    hash = JLUSER.hash
            WHERE userID = JLUSER.userID;
    END IF; 
END;
/

CREATE OR REPLACE FUNCTION getJLUSER(ID IN VARCHAR2) RETURN SYS_REFCURSOR IS
    cur SYS_REFCURSOR;
BEGIN
        OPEN cur FOR
            SELECT USERID AS "userID",
            FAILEDLOGINCOUNT AS "failedLogin",
            SALT AS "salt",
            HASH AS "hash"
            FROM JLUSERS
            WHERE USERID = id;
        return cur;
END;
/
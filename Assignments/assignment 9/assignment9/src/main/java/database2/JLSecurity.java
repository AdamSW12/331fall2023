package database2;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import oracle.jdbc.OracleTypes;

public class JLSecurity implements IJLSecurity{

    private Connection conn;

    @Override
    public void CreateUser(String user, String password){
        SecureRandom sr;
        byte[] salt = new byte[16];
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
            sr.nextBytes(salt);
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 5, 30 * 8);
            SecretKeyFactory skf =
            SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            User newUser = new User(user, 0, salt, hash);
            updateDB(newUser);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean Login(String username, String password) {
        boolean same = false;
        try {
            User user = (User) getUser(username);
            long failures = user.getFailedLoginCount();
            PBEKeySpec check_spec = new PBEKeySpec(password.toCharArray(),user.getSalt(), 5, user.getHash().length * 8);
            SecretKeyFactory check_skf =
            SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] check_hash =
            check_skf.generateSecret(check_spec).getEncoded();
            same = Arrays.equals(user.getHash(),check_hash);

            if(!same){
                failures +=1;
                user.setFailedLoginCount(failures);
                user.addToDatabase(conn);
            }
            if(failures >= 5){
                throw new IllegalAccessError("Too many failed login attempts");
            }
            return same;
        } catch (NoSuchAlgorithmException NSAE) {
            NSAE.printStackTrace();
        } catch (InvalidKeySpecException IKSE) {
            IKSE.printStackTrace();
        }
        return same;
    }

    public void setConnection(Connection conn){
        this.conn = conn;
    }


    private IUser getUser(String userName){
        IUser user = new User();
        String sql = "{? = call getJLUSER(?)}";
        try(CallableStatement statement = conn.prepareCall(sql)){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.setString(2,userName);
            statement.executeQuery();
            ResultSet rs = (ResultSet)statement.getObject(1);

            while(rs.next()){
                user = new User(
                    rs.getString("userID"),
                    rs.getLong("failedLogin"),
                    rs.getBytes("salt"),
                    rs.getBytes("hash")
                );
            }
        }catch(SQLException e){
            e.printStackTrace();
        }

        return user;
    }

    private void updateDB(IUser user){
        ((User) user).addToDatabase(conn);
    }
}

package database2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class App 
{
    static JLSecurity security = new JLSecurity();
    static Scanner in = new Scanner(System.in);
    public static void main( String[] args )
    {
        try {
            Connection conn = getConnection();
            security.setConnection(conn);
            userActions(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            in.close();
        }

    }

    public static void userActions(Connection conn){
        List<String> actions = new ArrayList<>();
        in = new Scanner(System.in);

        actions.add("(1) create user");
        actions.add("(2) login");
        actions.add("(3) end");

        System.out.println("\nWhat do you want to do?");
        for (String action : actions) {
            System.out.println(action);
        }
        int userAction = -1;
        try{
            userAction = Integer.parseInt(in.nextLine());
        }catch(NumberFormatException NFE){
            System.out.println("not a number");
            userActions(conn);
        }
        
        switch (userAction) {
            case 1:
                createUser();
                break;
            case 2:
                loginUser();
                break;
            case 3:
                System.exit(0);
            default:
                System.out.println("Invalid action");
                userActions(conn);
        }
        userActions(conn);
    }
    
    private static void createUser() {
        in = new Scanner(System.in);
        System.out.print("Enter the username: ");
        String userName = in.nextLine();

        System.out.print("Enter the password: ");
        String userPWD = in.nextLine();
        security.CreateUser(userName,userPWD);
        return;
    }
    
    private static void loginUser(){
        in = new Scanner(System.in);
        System.out.print("Enter the username: ");
        String userName = in.nextLine();

        System.out.print("Enter the password: ");
        String userPWD = in.nextLine();

        try{
            if(security.Login(userName, userPWD)){
                System.out.println("successfully logged in");
            } else{
                System.out.println("Could not login");
            }
        }catch(IllegalAccessError IAE){
            System.out.println(IAE.getMessage());
        }

    }

    public static Connection getConnection() throws SQLException{
        in = new Scanner(System.in);
        System.out.print("Username: ");
        String userName = in.nextLine();

        System.out.print("Password: ");
        String userPWD = in.nextLine();

        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn =
        DriverManager.getConnection(url, userName, userPWD);

        return conn;
    }
}

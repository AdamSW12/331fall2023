package database2;

import java.sql.*;
import java.util.Map;

public class User implements IUser,SQLData{
    public final String type_name = "JLUSERS_TYPE";
    private String userName;
    private long FailedLoginCount;
    private byte[] salt;
    private byte[] hash;
    
    public User(String userName, long failedLoginCount, byte[] salt, byte[] hash) {
        this.userName = userName;
        this.FailedLoginCount = failedLoginCount;
        this.salt = salt;
        this.hash = hash;
    }
    
    public User() {
    }

    @Override
    public byte[] getSalt() {
        return this.salt;
    }

    public void setSalt(byte[] salt){
        this.salt = salt;
    }
    
    @Override
    public String getUser() {
        return this.userName;
    }

    public void setUser(String userName) {
        this.userName = userName;
    }
    
    @Override
    public byte[] getHash() {
        return this.hash;
    }
    public void setHash(byte[] hash){
        this.hash = hash;
    }

    @Override
    public long getFailedLoginCount() {
        return this.FailedLoginCount;
    }
    
    @Override
    public void setFailedLoginCount(long count) {
        this.FailedLoginCount = count;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return this.type_name;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setUser(stream.readString());
        setFailedLoginCount(stream.readLong());
        setSalt(stream.readBytes());
        setHash(stream.readBytes());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getUser());
        stream.writeLong(getFailedLoginCount());
        stream.writeBytes(getSalt());
        stream.writeBytes(getHash());
    }

    public void addToDatabase(Connection conn){
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put("JLUSERS_TYPE", "database2.User");
            String sql = "{call createJLUSER(?)}";
            try(CallableStatement statement = conn.prepareCall(sql)){
                statement.setObject(1,this);
                statement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}


CREATE OR REPLACE TYPE Order_Array_Type IS VARRAY(100) OF NUMBER;
/

create or replace package book_store AS
    FUNCTION get_price_after_tax(book_isbn varchar2)
        return NUMBER;
    TYPE Order_Array_Type IS VARRAY(100) OF NUMBER;
    PROCEDURE show_purchasers;

    --assignment 4
    procedure rename_category(cat_old_name IN varchar2, cat_new_name IN varchar2);
    category_not_found exception;
    cat_name_too_long exception;

    procedure add_publisher(PUBname IN varchar2, contact IN VARCHAR2, phone IN VARCHAR2);
    publisher_already_exists exception;
end book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS

    FUNCTION price_after_discount(book_isbn VARCHAR2)
        return NUMBER IS
            Discount_price NUMBER(5,2);
        BEGIN
            SELECT (b.retail - b.DISCOUNT) INTO Discount_price FROM books b
            WHERE b.ISBN = book_isbn;
            return Discount_price;
        end;

    FUNCTION get_price_after_tax(book_isbn varchar2)
        RETURN NUMBER IS
            final_cost NUMBER;
            Discount_price NUMBER;
        BEGIN
            discount_price := price_after_discount(book_isbn);
            final_cost := discount_price + (Discount_price * 0.15);
            RETURN final_cost;
        
    END;

    FUNCTION book_purchasers(book_isbn varchar2)
    RETURN Order_Array_Type IS
        Order_Array Order_Array_Type;
    BEGIN
        SELECT o.customer# BULK COLLECT INTO Order_array FROM ORDERS o
        INNER JOIN ORDERITEMS ob USING (ORDER#)
        WHERE ob.ISBN = book_isbn;
        return Order_array;
    end;

    PROCEDURE show_purchasers IS
        customer#_arr book_store.Order_Array_Type;
        customer_Name varchar2(30);
    BEGIN
        FOR arow IN (SELECT * FROM BOOKS) LOOP
            CUSTOMER#_arr := BOOK_STORE.book_purchasers(arow.ISBN);
            
            DBMS_OUTPUT.PUT_LINE('-----------------------------------------');
            DBMS_OUTPUT.PUT_LINE('ISBN: ' || arow.isbn || ', Title: ' || arow.title || ', Names: ' );

            FOR i in 1.. customer#_arr.COUNT LOOP

                SELECT firstname || ' ' || lastname into customer_Name 
                from customers
                where customer# = customer#_arr(i);
                DBMS_OUTPUT.PUT(customer_Name || ', ');

            end loop;
            DBMS_OUTPUT.NEW_LINE();

        end loop;

    END;
    --assignment 4
    procedure rename_category(cat_old_name IN VARCHAR2,cat_new_name IN varchar2) IS
        BEGIN
            if LENGTH(cat_new_name) > 12 THEN
                RAISE BOOK_STORE.cat_name_too_long;
            END IF;

            UPDATE BOOKS
            SET CATEGORY = cat_new_name
            WHERE category = cat_old_name;

            IF SQL%NOTFOUND THEN
                RAISE BOOK_STORE.category_not_found;
            END IF;
    END;

    procedure add_publisher(PUBname IN varchar2, contact IN VARCHAR2, phone IN VARCHAR2) IS
        pub_exists number(1);
        last_id number(2);
        BEGIN
            SELECT COUNT(*) INTO pub_exists FROM publisher p where p.name = PUBname;
            SELECT MAX(PUBID) INTO last_id FROM publisher;

            if pub_exists = 1 THEN
                RAISE BOOK_STORE.publisher_already_exists;
            else
                INSERT INTO PUBLISHER(PUBID, name, CONTACT, PHONE)
                VALUES (last_id + 1, PUBname, contact, phone);
            END IF;
        END;


END book_store;
/

DECLARE
    result NUMBER(5,2);
    book_isbn NUMBER(10);
    order_arr BOOK_STORE.Order_Array_Type;
BEGIN
    SELECT b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'BUILDING A CAR WITH TOOTHPICKS';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of BUILDING A CAR WITH TOOTHPICKS: ' || result);

    SELECT b.isbn INTO book_ISBN FROM BOOKS b WHERE b.TITLE = 'HOLY GRAIL OF ORACLE';
    result := book_store.get_price_after_tax(book_isbn);
    DBMS_OUTPUT.put_line('Final price of HOLY GRAIL OF ORACLE: ' || result);
    

    BOOK_STORE.show_purchasers;

    --assignment 4
    DBMS_OUTPUT.PUT_LINE('-----------------------------------------');
    DBMS_OUTPUT.PUT_LINE('Assignment 4: ');
    BOOK_STORE.rename_category('COMPUTER','COMPUTER SCIENCE');

EXCEPTION
    WHEN BOOK_STORE.cat_name_too_long THEN
        DBMS_OUTPUT.PUT_LINE('Category name too long. Limit: 12 characters');
    WHEN BOOK_STORE.category_not_found THEN
        DBMS_OUTPUT.PUT_LINE('Category not found');
    WHEN BOOK_STORE.publisher_already_exists THEN
        DBMS_OUTPUT.PUT_LINE('Publisher already exists');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error');
end;
/

BEGIN
    BOOK_STORE.rename_category('teaching','education');
EXCEPTION
    WHEN BOOK_STORE.cat_name_too_long THEN
        DBMS_OUTPUT.PUT_LINE('Category name too long. Limit: 12 characters');
    WHEN BOOK_STORE.category_not_found THEN
        DBMS_OUTPUT.PUT_LINE('Category not found');
END;
/

BEGIN
    DBMS_OUTPUT.PUT_LINE('-----------------------------------------');
    DBMS_OUTPUT.PUT_LINE('Inserting publishers: ');
    BOOK_STORE.add_publisher('Dawson Printing', 'John Smith', '111-555-2233');
    BOOK_STORE.add_publisher('Publish Our Way', 'Jane Tomlin', '010-410-0010');
EXCEPTION
    WHEN BOOK_STORE.cat_name_too_long THEN
        DBMS_OUTPUT.PUT_LINE('Category name too long. Limit: 12 characters');
    WHEN BOOK_STORE.category_not_found THEN
        DBMS_OUTPUT.PUT_LINE('Category not found');
    WHEN BOOK_STORE.publisher_already_exists THEN
        DBMS_OUTPUT.PUT_LINE('Publisher already exists');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error');
END;
/
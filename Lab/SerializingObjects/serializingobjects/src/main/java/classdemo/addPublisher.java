package classdemo;

import java.sql.*;
import java.util.Scanner;
import java.util.Map;

public class addPublisher 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        Connection conn = getConnection();
        Publisher myPublisher = new Publisher(6,"stanINC","Stan","800-555-2326");
        // Map map = conn.getTypeMap();
        // conn.setTypeMap(map);
        // map.put("PUBLISHER_TYPE", Class.forName("classdemo.Publisher"));
        
        // String sql = "{call add_publisher(?)}";
        // try(CallableStatement stmt = conn.prepareCall(sql)){
        //     stmt.setObject(1,myPublisher);
        //     stmt.execute();
        // }
       addPub(myPublisher, conn);
       Publisher pub = getPublisher(myPublisher, conn);
       System.out.println(pub);
    }

    public static void addPub(Publisher myPublisher, Connection conn) throws SQLException, ClassNotFoundException{
        Map map = conn.getTypeMap();
        conn.setTypeMap(map);
        map.put("PUBLISHER_TYPE", Class.forName("classdemo.Publisher"));
        
        String sql = "{call add_publisher(?)}";
        try(CallableStatement stmt = conn.prepareCall(sql)){
            stmt.setObject(1,myPublisher);
            stmt.execute();
        }
            
    }
    
    public static Publisher getPublisher(Publisher myPublisher, Connection conn) throws SQLException{
        Publisher returnPublisher = new Publisher();
        PreparedStatement statement = conn.prepareStatement("select * from publisher where pubID = ?");
        statement.setInt(1, myPublisher.getPubID());
        ResultSet result = statement.executeQuery();

        while(result.next()){
            returnPublisher.setPubID(result.getInt("pubid"));
            returnPublisher.setPubName(result.getString("name"));
            returnPublisher.setPubContact(result.getString("contact"));
            returnPublisher.setPubPhone(result.getString("phone"));
        }
        return returnPublisher;
    }
    public static Connection getConnection() throws SQLException{
        Scanner in = new Scanner(System.in);
        System.out.print("Username: ");
        String userName = in.next();

        System.out.print("Password: ");
        String userPWD = in.next();

        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn =
        DriverManager.getConnection(url, userName, userPWD);

        in.close();
        return conn;
    }
}

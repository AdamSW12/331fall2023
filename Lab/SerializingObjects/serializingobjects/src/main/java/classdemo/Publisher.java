package classdemo;
import java.sql.*;

public class Publisher implements SQLData{

    public static final String type_name = "PUBLISHER_TYPE";
    private int pubID;
    private String pubName;
    private String pubContact;
    private String pubPhone;

    public Publisher(int pubID, String pubName, String pubContact, String pubPhone) throws SQLException {
        checkData(pubName,23);
        checkData(pubContact,15);
        checkData(pubPhone,12);

        this.pubID = pubID;
        this.pubName = pubName;
        this.pubContact = pubContact;
        this.pubPhone = pubPhone;
    }

    public Publisher(){}

    private void checkData(String data, int limit){
        if(data.length()<=limit){
            return;
        }
        throw new IllegalArgumentException(data + " too long, max length: " + data); 
    }

    public int getPubID() {
        return pubID;
    }
    public String getPubName() {
        return pubName;
    }
    public String getPubContact() {
        return pubContact;
    }
    public String getPubPhone() {
        return pubPhone;
    }
    public void setPubID(int pubID) {
        this.pubID = pubID;
    }

    public void setPubName(String pubName) {
        this.pubName = pubName;
    }

    public void setPubContact(String pubContact) {
        this.pubContact = pubContact;
    }

    public void setPubPhone(String pubPhone) {
        this.pubPhone = pubPhone;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return type_name;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setPubID(stream.readInt());
        setPubName(stream.readString());
        setPubContact(stream.readString());
        setPubPhone(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getPubID());
        stream.writeString(getPubName());
        stream.writeString(getPubContact());
        stream.writeString(getPubPhone());
    }
    
    @Override
    public String toString() {
        return "Publisher [pubID=" + pubID + ", pubName=" + pubName + ", pubContact=" + pubContact + ", pubPhone="
                + pubPhone + "]";
    }
}

DROP procedure add_publisher;
DROP TYPE publisher_type;
/

create type publisher_type as object(
    PUBID number(2),
    NAME VARCHAR2(23),
    CONTACT VARCHAR2(15),
    PHONE CHAR(12)
);
/

CREATE OR REPLACE PROCEDURE add_publisher(
    vPublisher in publisher_type
) IS
BEGIN
    insert into PUBLISHER VALUES(
        vPublisher.PUBID,
        vPublisher.NAME,
        vPublisher.CONTACT,
        vPublisher.PHONE
    );
END;
/